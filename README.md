# Holition Frontend Test

This test is about building a folder viewer similar in style to google drive from the data in a json file.

The ui images attached are for guidance, they don't need to be copied exactly (indeed they are missing file sizes).

## Routing
The page should using routing following the below scheme.

/drive/[folder]...

|  Examples |
|---|
| /drive/ |
| /drive/images |
| /drive/images/hello.png |
| /drive/doc |
| /drive/doc/list.txt |`

## Accessibility
The page should follow standard accessibility
use of semantic HTML elements, tabbing etc...

## Responsiveness
The page should on mobile show a table like view, on desktop a more icon first view.
### Mobile
![](/doc/ui/drive_02.png)
### Desktop
![](/doc/ui/drive_01.png)

## Animations
If possible subtle animations should be added on hover, and if possible on route transitions.


## The file data.
When running the app with `npm run serve:app+data`

It will serve the data locally, load in db.json from http://localhost:3000/files

There should be a loading page while data is fetched.

Included in the json data is file sizes in bytes, these should be displayed in the UI in a human friendly format, for example 1,000,000 becomes 1MB, 1,500 becomes 1.5KB, this should be trucated such that 1,000,500 simply shows as 1MB, not 1.0005MB.

The actual files can be found in assets, fetched under the angular app

## Example UI

![](/doc/ui/drive_00.png)
![](/doc/ui/drive_01.png)
![](/doc/ui/drive_02.png)
![](/doc/ui/drive_03.png)
![](/doc/ui/drive_04.png)

